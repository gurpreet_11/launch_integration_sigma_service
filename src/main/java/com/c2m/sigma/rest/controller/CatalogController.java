package com.c2m.sigma.rest.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.c2m.sigma.catalog.model.GroupEndModelWrapper;
import com.c2m.sigma.catalog.model.GroupStartModelWrapper;
import com.c2m.sigma.catalog.model.NotificationModelWrapper;
import com.c2m.sigma.catalog.model.NotificationReply;
import com.c2m.sigma.catalog.service.SendNotificationClient;

@RestController
public class CatalogController {
	
	
	protected Logger logger = Logger.getLogger(CatalogController.class.getName());

	@RequestMapping(value = "/sendNotification", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.TEXT_XML_VALUE })
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public NotificationReply sendNotification(@Validated @RequestBody NotificationModelWrapper notificationModelWrapper,
			Errors errors) {

		NotificationReply notificationReply = new NotificationReply();
		notificationReply.setResult("true");
		if (errors.hasErrors()) {
			logger.debug("Bad Request:Error Occured in sendNotification API");
			notificationReply.setResult("false");
			notificationReply.setFailureMessage(errors.getFieldErrors().get(0).getDefaultMessage());
		}

		return notificationReply;
	}

	@RequestMapping(value = "/groupStart", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.TEXT_XML_VALUE })
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public NotificationReply groupStart(@Validated @RequestBody GroupStartModelWrapper groupStartModelWrapper,
			Errors errors) {

		NotificationReply notificationReply = new NotificationReply();
		notificationReply.setResult("true");
		if (errors.hasErrors()) {
			logger.debug("Bad Request:Error Occured in groupStart API");
			notificationReply.setResult("false");
			notificationReply.setFailureMessage(errors.getFieldErrors().get(0).getDefaultMessage());
		}

		return notificationReply;
	}
	
	@RequestMapping(value = "/groupEnd", method = RequestMethod.POST , consumes = MediaType.APPLICATION_JSON_VALUE ,produces = { MediaType.APPLICATION_XML_VALUE, MediaType.TEXT_XML_VALUE })
	@ResponseBody
	public NotificationReply groupEnd(@Validated @RequestBody GroupEndModelWrapper groupEndModelWrapper , Errors errors,HttpServletResponse response) {
		
		System.out.println("Launch Origin public id ::"+groupEndModelWrapper.getLaunchOriginatorModel().getPublicId());
		System.out.println("Launch Origin Type path::"+groupEndModelWrapper.getLaunchOriginatorModel().getTypepath());
		
		System.out.println("Catalog Instance Id ::"+groupEndModelWrapper.getCatalogInstanceID());
		
		//sendNotificationClient.sendNotifcation(notificationModelWrapper);
		
		NotificationReply notificationReply = new NotificationReply();
		notificationReply.setResult("true");
		if(errors.hasErrors()) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			logger.debug("Bad Request:Error Occured in groupStart API");
			notificationReply.setResult("false");
			notificationReply.setFailureMessage(errors.getFieldErrors().get(0).getDefaultMessage());
		}
		else{
			response.setStatus(HttpServletResponse.SC_OK);
		}
		
		return notificationReply;
	}
}
