package com.c2m.sigma.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@EnableCircuitBreaker
@EnableDiscoveryClient
@ComponentScan({"com.c2m.sigma"})
@SpringBootApplication(scanBasePackages = {"com.c2m.sigma.rest"})
public class C2mRestServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(C2mRestServiceApplication.class, args);
	}
}
