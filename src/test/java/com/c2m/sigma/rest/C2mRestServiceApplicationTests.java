package com.c2m.sigma.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.c2m.sigma.rest.controller.CatalogController;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(CatalogController.class)
public class C2mRestServiceApplicationTests {

	private static final String notificationJson =			
			 "{" +  
				   "\"LaunchGuid\":\"58ffa2a0-26d4-434d-8d1c-932431950cf6\"," +
				   "\"LaunchDate\":\"2017-10-23T11:52:56.38\"," +
				   "\"Entity\":{" +  
					   "\"PublicId\":\"7dd1dad5-ef4b-442a-91db-bc15155d0338\"," +
					   "\"BusinessId\":\"00000001\"," +
					   "\"Name\":\"Test Launch Package [NAME CHANGE]\"," +
					   "\"ClassType\":\"Package_Template\"," +
					   "\"TypePath\":\"Launch_Entity\\\\Product\\\\Package\\\\Package_Template\"," +
					   "\"LaunchReason\":2,"+
					   "\"ExtensionData\":null"+
				   "}," +
				   "\"LaunchOriginator\":{"+  
					   "\"PublicId\":\"da321db0-905d-4609-afae-2101f27aef2d\"," +
					   "\"Name\":\"Demo Launch Integration Strategy\"," +
					   "\"BusinessId\":\"00000011\"," +
					   "\"ClassType\":\"First_Template\"," +
					   "\"TypePath\":\"Change_Entity\\\\Change_Set\\\\First_Template\"," +
					   "\"ExtensionData\":null"+
				   "}," +
				   "\"LaunchGroup\":{"+  
					   "\"Guid\":\"a08b821b-11ee-467e-b934-f465b0d81e2b\"," +
					   "\"EntityCount\":3,"+
					   "\"TotalEntityCount\":3,"+
					   "\"ExtensionData\":null"+
				   "}," +
				   "\"CatalogInstanceID\":\" \"," +
				   "\"Information\":["+  
				   "{" +   
					   	"\"Name\":\"LaunchUser\"," +
					   	"\"Value\":\"ines martins\"," +
					   	"\"ExtensionData\":null"+
				    "}," +
				   "{" +   
				      "\"Name\":\"LaunchComments\"," +
				      "\"Value\":null,"+
				      "\"ExtensionData\":null"+
				    "}" +
				   "]," +
				 "\"ExtensionData\":null" +
				"}";	
	private static final String groupStartJson ="{  \r\n   \"StartDateTime\":\"2017-10-23T11:52:56.377\",\r\n   \"LaunchGroup\":{  \r\n      \"Guid\":\"a08b821b-11ee-467e-b934-f465b0d81e2b\",\r\n      \"TotalEntityCount\":3,\r\n      \"ExtensionData\":null\r\n   },\r\n   \"LaunchOriginator\":{  \r\n      \"PublicId\":\"da321db0-905d-4609-afae-2101f27aef2d\",\r\n      \"Name\":\"Demo Launch Integration Strategy\",\r\n      \"BusinessId\":\"00000011\",\r\n      \"ClassType\":\"First_Template\",\r\n      \"TypePath\":\"Change_Entity\\\\Change_Set\\\\First_Template\",\r\n      \"ExtensionData\":null\r\n   },\r\n   \"CatalogInstanceID\":\"10fb5edc-dbe3-84b1-9951-c482eb866a54\",\r\n   \"Information\":[  \r\n      {  \r\n         \"Name\":\"LaunchUser\",\r\n         \"Value\":\"ines martins\",\r\n         \"ExtensionData\":null\r\n      },\r\n      {  \r\n         \"Name\":\"LaunchComments\",\r\n         \"Value\":null,\r\n         \"ExtensionData\":null\r\n      }\r\n   ],\r\n   \"ExtensionData\":null\r\n}";
	private static final String groupEndJson ="{  \r\n   \"CompletedDateTime\":\"2017-10-23T00:56:19.9555932Z\",\r\n   \"Successful\":true,\r\n   \"LaunchGroup\":{  \r\n      \"Guid\":\"a08b821b-11ee-467e-b934-f465b0d81e2b\",\r\n      \"TotalEntityCount\":3,\r\n      \"SuccessfulCount\":3,\r\n      \"ExtensionData\":null\r\n   },\r\n   \"LaunchOriginator\":{  \r\n      \"PublicId\":\"da321db0-905d-4609-afae-2101f27aef2d\",\r\n      \"Name\":\"Demo Launch Integration Strategy\",\r\n      \"BusinessId\":\"00000011\",\r\n      \"ClassType\":\"First_Template\",\r\n      \"TypePath\":\"Change_Entity\\\\Change_Set\\\\First_Template\",\r\n      \"ExtensionData\":null\r\n   },\r\n   \"CatalogInstanceID\":\"10fb5edc-dbe3-84b1-9951-c482eb866a54\",\r\n   \"Information\":[  \r\n      {  \r\n         \"Name\":\"LaunchUser\",\r\n         \"Value\":\"ines martins\",\r\n         \"ExtensionData\":null\r\n      },\r\n      {  \r\n         \"Name\":\"LaunchComments\",\r\n         \"Value\":null,\r\n         \"ExtensionData\":null\r\n      }\r\n   ],\r\n   \"ExtensionData\":null\r\n}";

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	ObjectMapper objectMapper;
	
	
	@Test
	public void testSendNotification() throws Exception {
		 
		System.out.println("Entity launch Reason::"+notificationJson);
		this.mockMvc.perform(post("/sendNotification")
		 .contentType(MediaType.APPLICATION_JSON_VALUE)
		 .content(notificationJson))
         .andExpect(status().isOk())
         .andExpect(content().contentType("application/xml"));
	}
	
	@Test
	public void testGroupStart() throws Exception {
		 
		System.out.println("Entity launch Reason::"+notificationJson);
		this.mockMvc.perform(post("/groupStart")
		 .contentType(MediaType.APPLICATION_JSON_VALUE)
		 .content(groupStartJson))
         .andExpect(status().isOk())
         .andExpect(content().contentType("application/json"));
	}
	
	@Test
	public void testGroupEnd() throws Exception {
		 
		System.out.println("Entity launch Reason::"+notificationJson);
		this.mockMvc.perform(post("/groupEnd")
		 .contentType(MediaType.APPLICATION_JSON_VALUE)
		 .content(groupEndJson))
         .andExpect(status().isOk())
         .andExpect(content().contentType("application/json"));
	}

}
